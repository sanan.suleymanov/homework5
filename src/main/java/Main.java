package Homework4;

public class Main {
    public static void main(String[] args) {
        String[] habits = {"run", "eat", "sleep"};
        String [][] schedule = {{"Sunday","Code"},{"Monday","Code"}};

        Homework4.Human mother = new Homework4.Human("Katerina", "Snowden", 1968);
        Homework4.Human father = new Homework4.Human("Edward", "Snowden", 1956);
        Homework4.Pet pet = new Homework4.Pet("dog","Alabash",3,100, habits);
        Homework4.Human child = new Homework4.Human("Steve","Snowden",1993,120,pet,mother,father,schedule);

        System.out.println(pet.toString());
        System.out.println(child.toString());


    }
}